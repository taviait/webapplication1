﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Databases;
using WebApplication1.Models;

namespace WebApplication1.Services
{
  public class UserService : IUserService
  {
    public DataContext DataContext { get; }
    public UserService(DataContext dataContext)
    {
      DataContext = dataContext;
    }

    public List<User> GetAll()
    {
      return DataContext.Users.ToList();
    }

    public void AddUser(User user)
    {
      DataContext.Users.Add(user);
      DataContext.SaveChanges();
    }
  }
}
