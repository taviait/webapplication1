﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Responses;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
  [Route("api/users")]
  [ApiController]
  public class UserController : ControllerBase
  {
    public IUserService UserService { get; }
    public UserController(IUserService userService)
    {
      UserService = userService;
    }
    // GET: api/users
    [HttpGet]
    [Route("")]
    public IActionResult GetAllUser()
    {
      return Ok(new APIResponses
      {
        Code = 200,
        Message = "Success",
        Data = UserService.GetAll()
      });
    }

    // POST: api/User
    [HttpPost]
    [Route("add")]
    public IActionResult AddUser([FromBody] User user)
    {
      if (ModelState.IsValid)
      {
        UserService.AddUser(user);
        return Ok(new APIResponses
        {
          Code = 200,
          Message = "Success",
          Data = null
        });
      }
      else
      {
        return BadRequest(new APIResponses
        {
          Code = 400,
          Message = "Something error!",
          Data = null
        });
      }
    }
  }
}
