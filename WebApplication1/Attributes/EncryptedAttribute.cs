﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Attributes
{
  [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
  sealed class EncryptedAttribute : Attribute
  {
  }
}
