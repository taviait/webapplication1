﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Configure;
using WebApplication1.Attributes;


namespace WebApplication1.Models
{
  [Table("AspNetUsers")]
  public class User
  {
    public long ID { get; set; }
    public string Username { get; set; }

    [Encrypted]
    public string Password { get; set; }

    [Encrypted]
    public string CardID { get; set; }
  }
}
