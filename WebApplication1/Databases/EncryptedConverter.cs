﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApplication1.Configure;

namespace WebApplication1.Databases
{
  class EncryptedConverter : ValueConverter<string, string>
  {
    static readonly string Key = "b14ca5898a4e4133bbce2ea2315a1916";

    public EncryptedConverter(ConverterMappingHints mappingHints = default)
    : base(EncryptExpr, DecryptExpr, mappingHints)
    { }

    static Expression<Func<string, string>> DecryptExpr = x => SymmetricCryptConfig.DecryptString(Key, x);
    static Expression<Func<string, string>> EncryptExpr = x => SymmetricCryptConfig.EncryptString(Key, x);
  }
}
