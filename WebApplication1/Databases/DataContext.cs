﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Attributes;
using WebApplication1.Configure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Linq.Expressions;

namespace WebApplication1.Databases
{
  public class DataContext : DbContext
  {

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }
    public DbSet<User> Users { get; set; }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      //modelBuilder.Entity<User>()
      //  .Property(x => x.Password).HasMaxLength(20);

      foreach (var entityType in modelBuilder.Model.GetEntityTypes())
      {
        foreach (var property in entityType.GetProperties())
        {
          var attributes = property.PropertyInfo.GetCustomAttributes(typeof(EncryptedAttribute), false);
          if (attributes.Any())
          {
            property.SetValueConverter(new EncryptedConverter());
          }
        }
      }
    }
  }
}

