﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Configure
{
  static class ReverseCryptConfig
  {
    public static string Decrypt(string s) => new string(s.Reverse().ToArray());

    public static string Encrypt(string s) => new string(s.Reverse().ToArray());
  }
}
